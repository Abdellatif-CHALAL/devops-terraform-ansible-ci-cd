resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_internet_gateway" "my_ig" {
  vpc_id = aws_vpc.my_vpc.id
}


resource "aws_route_table" "my_route_table" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_ig.id
  }
}

resource "aws_subnet" "my_subnet" {
  vpc_id            = aws_vpc.my_vpc.id
  availability_zone = "eu-west-1a"
  cidr_block        = "10.0.1.0/24"

  tags = {
    Projet = "DevOps"
  }
}

resource "aws_route_table_association" "my_route_table_association" {
  subnet_id      = aws_subnet.my_subnet.id
  route_table_id = aws_route_table.my_route_table.id
}


resource "aws_security_group" "my_security_group" {
  name_prefix = "AKIAQFWWW3BY757JOLEQ"
  vpc_id      = aws_vpc.my_vpc.id

  tags = {
    Projet = "DevOps"
  }
}

resource "aws_security_group_rule" "my_security_group_rule_out_http" {
  type              = "egress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_security_group.id
}


resource "aws_instance" "web" {
  ami           = "ami-0e3f7a235a05f8e99"
  instance_type = "t2.micro"

  tags = {
    Name      = "chalal.abdellatif"
    Formation = "terraform"
    TP        = "TP1"
  }
}
